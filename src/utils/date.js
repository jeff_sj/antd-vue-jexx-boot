/**
 * 时间格式化
 * @param value date
 * @param fmt 格式
 * @returns {*}
 */
export function formatDate (value, fmt) {
  const getDate = value
  const o = {
    'M+': getDate.getMonth() + 1,
    'd+': getDate.getDate(),
    'h+': getDate.getHours(),
    'm+': getDate.getMinutes(),
    's+': getDate.getSeconds(),
    'q+': Math.floor((getDate.getMonth() + 3) / 3),
    'S': getDate.getMilliseconds()
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (getDate.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (const k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt
}

/**
 * 获取某天所在月的天数
 * @param value 时间
 * @returns 月天数
 */
export function getMonthDayNum (value) {
  const year = value.getFullYear()
  const month = value.getMonth()
  const monthStartDate = new Date(year, month, 1)
  const monthEndDate = new Date(year, month + 1, 1)
  const days = (monthEndDate - monthStartDate) / (1000 * 60 * 60 * 24)
  return days
}

/**
 * 获取某天的开始时间
 * @param value 某天
 * @returns 某天的开始时间
 */
export function getStartTimeOfDate (value) {
  const year = value.getFullYear()
  const month = value.getMonth()
  const dateOfMonth = value.getDate()
  return new Date(year, month, dateOfMonth, 0, 0, 0)
}

/**
 * 获取某天的结束时间
 * @param value 某天
 * @returns 某天的结束时间
 */
export function getEndTimeOfDate (value) {
  const year = value.getFullYear()
  const month = value.getMonth()
  const dateOfMonth = value.getDate()
  return new Date(year, month, dateOfMonth, 23, 59, 59)
}

/**
 * 获取某天所在周的开始时间。<br/>
 *  date对象默认是按星期天开始的，因此计算时需把星期天当成最后一天。
 * @param value 某天
 * @returns 周开始时间
 */
export function getStartTimeOfWeek (value) {
  const year = value.getFullYear()
  const month = value.getMonth()
  const dateOfMonth = value.getDate()
  const dateOfWeeb = value.getDay() === 0 ? 7 : value.getDay()
  return new Date(year, month, dateOfMonth - dateOfWeeb + 1, 0, 0, 0)
}

/**
 * 获取某天所在周的结束时间。<br/>
 *  date对象默认是按星期天开始的，因此计算时需把星期天当成最后一天。
 * @param value 某天
 * @returns 周结束时间
 */
export function getEndTimeOfWeek (value) {
  const year = value.getFullYear()
  const month = value.getMonth()
  const dateOfMonth = value.getDate()
  const dateOfWeeb = value.getDay() === 0 ? 7 : value.getDay()
  return new Date(year, month, dateOfMonth - dateOfWeeb + 7, 23, 59, 59)
}

/**
 * 获取某天所在月的开始时间
 * @param value 某天
 * @returns 月开始时间
 */
export function getStartTimeOfMonth (value) {
  const year = value.getFullYear()
  const month = value.getMonth()
  return new Date(year, month, 1, 0, 0, 0)
}

/**
 * 获取某天所在月的结束时间
 * @param value 某天
 * @returns 月结束时间
 */
export function getEndTimeOfMonth (value) {
  const year = value.getFullYear()
  const month = value.getMonth()
  return new Date(year, month, getMonthDayNum(value), 23, 59, 59)
}
