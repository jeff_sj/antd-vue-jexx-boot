import request from '@/utils/request'

const api = {
  user: '/sys/user',
  checkUsername: '/sys/user/checkUsername',
  changePassword: '/sys/user/changePassword',
  batchLockUser: '/sys/user/batchLock',
  batchUnlockUser: '/sys/user/batchUnlock',
  role: '/sys/role',
  service: '/service',
  permission: '/permission',
  permissionNoPager: '/permission/no-pager',
  orgTree: '/org/tree',
  fileView: '/sys/file/view',
  fileUpload: '/sys/file/upload'
}

const fileUpload1 = process.env.VUE_APP_API_BASE_URL + api.fileUpload
const fileView1 = process.env.VUE_APP_API_BASE_URL + api.fileView

export default api

export {
  api,
  fileUpload1,
  fileView1
}

export function getUserList (parameter) {
  return request({
    url: api.user,
    method: 'get',
    params: parameter
  })
}

export function getRoleList (parameter) {
  return request({
    url: api.role,
    method: 'get',
    params: parameter
  })
}

export function getServiceList (parameter) {
  return request({
    url: api.service,
    method: 'get',
    params: parameter
  })
}

export function getPermissions (parameter) {
  return request({
    url: api.permissionNoPager,
    method: 'get',
    params: parameter
  })
}

export function getOrgTree (parameter) {
  return request({
    url: api.orgTree,
    method: 'get',
    params: parameter
  })
}

// id == 0 add     post
// id != 0 update  put
export function saveService (parameter) {
  return request({
    url: api.service,
    method: parameter.id === 0 ? 'post' : 'put',
    data: parameter
  })
}

export function saveSub (sub) {
  return request({
    url: '/sub',
    method: sub.id === 0 ? 'post' : 'put',
    data: sub
  })
}

// ------------------------user

export function checkUsername (params) {
  return request({
    url: api.checkUsername,
    method: 'get',
    params: params
  })
}

export function changePassword (params) {
  return request({
    url: api.changePassword,
    method: 'post',
    params: params
  })
}

export function addUser (data) {
  return request({
    url: '/sys/user',
    method: 'post',
    data: data
  })
}

export function deleteUserById (userId) {
  return request({
    url: '/sys/user/' + userId,
    method: 'delete'
  })
}

export function deleteUsers (userIds) {
  return request({
    url: '/sys/user',
    method: 'delete',
    data: {
      userIds: userIds
    }
  })
}

export function updateUserById (userId, user) {
  return request({
    url: '/sys/user/' + userId,
    method: 'put',
    data: user
  })
}

export function lockUser (userId) {
  return request({
    url: '/sys/user/' + userId + '/lock',
    method: 'post'
  })
}

export function batchLockUser (userIds) {
  return request({
    url: '/sys/user/batchLock',
    method: 'post',
    params: {
      userIds: userIds
    }
  })
}

export function unlockUser (userId) {
  return request({
    url: '/sys/user/' + userId + '/unlock',
    method: 'post'
  })
}

export function batchUnLockUser (userIds) {
  return request({
    url: '/sys/user/batchUnlock',
    method: 'post',
    params: {
      userIds: userIds
    }
  })
}

// ------------------------role

export function addRole (role) {
  return request({
    url: '/sys/role',
    method: 'post',
    data: role
  })
}

export function deleteRoleById (roleId) {
  return request({
    url: '/sys/role/' + roleId,
    method: 'delete'
  })
}

export function batchDeleteRoles (roleIds) {
  return request({
    url: '/sys/role/',
    method: 'delete',
    params: {
      roleIds: roleIds
    }
  })
}

export function updateRoleById (roleId, role) {
  return request({
    url: '/sys/role/' + roleId,
    method: 'put',
    data: role
  })
}

export function pageRoles (parameter) {
  return request({
    url: '/sys/role',
    method: 'get',
    params: parameter
  })
}

export function getAllRoles (parameter) {
  return request({
    url: '/sys/role/list',
    method: 'get',
    params: parameter
  })
}

export function getUserRoles (userId) {
  return request({
    url: '/sys/role/user/' + userId,
    method: 'get'
  })
}

export function checkRoleCode (roleCode) {
  return request({
    url: '/sys/role/checkRoleCode',
    method: 'get',
    params: {
      code: roleCode
    }
  })
}

export function saveRoleResources (roleId, resourceIds) {
  return request({
    url: '/sys/role/' + roleId + '/resource',
    method: 'post',
    params: {
      resourceIds: resourceIds
    }
  })
}

// ------------------------ resource

export function getResourceById (id) {
  return request({
    url: '/sys/resource/' + id,
    method: 'get'
  })
}

export function getResourceTreeList () {
  return request({
    url: '/sys/resource/tree',
    method: 'get'
  })
}

export function getResourcesByRoleId (roleId) {
  return request({
    url: '/sys/resource/role/' + roleId,
    method: 'get'
  })
}

// ------------------------ dict

export function addDict (dict) {
  return request({
    url: '/sys/dict',
    method: 'post',
    data: dict
  })
}

export function deleteDictById (dictId) {
  return request({
    url: '/sys/dict/' + dictId,
    method: 'delete'
  })
}

export function updateDictById (dictId, dict) {
  return request({
    url: '/sys/dict/' + dictId,
    method: 'put',
    data: dict
  })
}

export function pageDicts (params) {
  return request({
    url: '/sys/dict',
    method: 'get',
    params: params
  })
}

// ------------------------ dictItem

export function addDictItem (dictId, item) {
  return request({
    url: '/sys/dictItem/dict/' + dictId,
    method: 'post',
    data: item
  })
}

export function deleteDictItemById (itemId) {
  return request({
    url: '/sys/dictItem/' + itemId,
    method: 'delete'
  })
}

export function batchDeleteDictItems (itemIds) {
  return request({
    url: '/sys/dictItem',
    method: 'delete',
    params: {
      dictItemIds: itemIds
    }
  })
}

export function updateDictItemById (itemId, item) {
  return request({
    url: '/sys/dictItem/' + itemId,
    method: 'put',
    data: item
  })
}

export function listDictItemByDictId (dictId) {
  return request({
    url: '/sys/dictItem/dict/' + dictId,
    method: 'get'
  })
}

export function pageDictItems (parameter) {
  return request({
    url: '/sys/dictItem',
    method: 'get',
    params: parameter
  })
}

// ------------------------ log

export function pageLogs (parameter) {
  return request({
    url: '/sys/log',
    method: 'get',
    params: parameter
  })
}
// -------------------- MenuList
export function getMenuList () {
  return request({
    url: '/sys/resource/menuTree',
    method: 'get'
  })
}
export function addMenu (parameter) {
  return request({
    url: '/sys/resource',
    method: 'post',
    data: parameter
  })
}
export function deleteMenu (id) {
  return request({
    url: '/sys/resource/' + id,
    method: 'delete'
  })
}
export function updateMenu (parameter) {
  return request({
    url: '/sys/resource/' + parameter.id,
    method: 'put',
    data: parameter
  })
}

// -------------------- task

export function addSysTask (task) {
  return request({
    url: '/sys/task',
    method: 'post',
    data: task
  })
}

export function deleteSysTaskById (id) {
  return request({
    url: '/sys/task/' + id,
    method: 'delete'
  })
}

export function updateSysTaskById (id, task) {
  return request({
    url: '/sys/task/' + id,
    method: 'put',
    data: task
  })
}

export function pageSysTasks (parameter) {
  return request({
    url: '/sys/task',
    method: 'get',
    params: parameter
  })
}

export function getSysTaskByCode (code) {
  return request({
    url: '/sys/task/getByCode',
    method: 'get',
    params: {
      code: code
    }
  })
}

export function startSysTask (taskId) {
  return request({
    url: '/sys/task/' + taskId + '/start',
    method: 'post'
  })
}

export function stopSysTask (taskId) {
  return request({
    url: '/sys/task/' + taskId + '/stop',
    method: 'post'
  })
}
